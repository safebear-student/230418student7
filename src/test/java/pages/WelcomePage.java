package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage {

    WebDriver driver;

    @FindBy(linkText = "Login")
    WebElement Login_Link;

    public WelcomePage(WebDriver _driver) {

        driver = _driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Welcome");
    }


    public void clickOnLogin() {
        Login_Link.click();

    }

}
